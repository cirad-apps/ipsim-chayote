#### Dentro de la pestaña "Sus datos":

1.	Responda las 15 preguntas relativas a sus prácticas agrícolas, a las condiciones ambientales y al entorno de su parcela.
2.	Valide presionando el botón validar.
3.	Automáticamente aparecerá la pestaña "Sus resultados".

------------------------------------------------------------------------

#### *¡Atención: los datos de entrada se utilizan para producir la simulación pero nunca son guardados!*

------------------------------------------------------------------------

#### Dentro de la pestaña "Sus resultados":

1.	En la parte superior, el modelo predice en una escala la intensidad del daño por moscas en la producción de chayote de su parcela.
2.	En la parte inferior, un diagrama ramificado indica las prácticas y condiciones que aumentan o reducen el daño.
