<style>
a {color: black;}
</style>


## Diez años de estudios sobre la gestión agroecológica de las moscas de la fruta en producción de chayote en la isla de La Reunión.

En el año 2009, se iniciaron en la isla una serie de estudios sobre la gestión agroecológica de las moscas de la fruta en cultivos de chayote. 
A partir del año 2000, el chayote, uno de los cultivos emblemáticos y patrimoniales de la isla de la Reunión, se vio seriamente amenazado por las moscas de la fruta: sus daños generaron pérdidas de producción y mermaron la productividad del cultivo.  


Estos estudios se enmarcaron en proyectos de Investigación y Desarrollo, que incluyeron una diversidad de organismos de Investigación (CIRAD, INRAe) y de Desarrollo (Cámara de Agricultura, organización Arifel y otras Organizaciones Profesionales), así como organizaciones de experimentación y apoyo en el área fitosanitaria (Armefhor, FDGDON).
Los agricultores productores de chayote fueron protagonistas clave desde un principio y a lo largo de lo que fue una experiencia agroecológica llevada a cabo a escala real, respaldada por todos los actores de la cadena de valor y bajo los auspicios de las autoridades públicas (DAAF Reunión).
Lo cual fue decisivo a la hora de transferir exitosamente los resultados y las innovaciones.  


Los resultados logrados han permitido proponer prácticas agroecológicas novedosas que limitan el uso de pesticidas, hasta el punto de que hoy en día, la mayoría de los cultivos de chayote de la isla manejan y comercializan en producción orgánica certificada.  


Para informar y capacitar a los productores y demás profesionales agrícolas, se ha concebido y puesto a punto un modelo para predecir la intensidad del nivel de daño ocasionado por las moscas de la fruta en los cultivos de chayote.


## IPSIM-chayote para simular el daño por moscas de la fruta en parcelas de chayote en La Reunión 

La elaboración de IPSIM-chayote fue coordinada por Jean-Philippe Deguine (CIRAD, UMR PVBMT).  

Las observaciones relativas a las moscas de la fruta fueron realizadas en el marco del proyecto Gamour bajo la dirección del CIRAD (UMR PVBMT) por Cédric Ajaguin-Soleyen, Morguen Atiama, Toulassi Atiama-Nurbel, Jean-Philippe Deguine, Elisabeth Douraguia, Thomas François, Cathy Jacquard, Marie-Ludders Moutoussamy, Aubéri Petite, Pascal Rousse y Chloé Schmitt.
Observaciones adicionales fueron realizadas por Marlène Marquier (FDGDON).  

El modelo IPSIM-chayote fue diseñado por Jean-Philippe Deguine (CIRAD, UMR PVBMT) y Jean-Noël Aubertot (INRAe, UMR AGIR), con contribuciones de Toulassi Atiama-Nurbel (Armeflhor), Baptiste Longoras (GAB), Marlène Marquier (FDGDON) y Luc Vanhuffel (Cámara de agricultura de la Reunión).
La contribución de Arnaud Allègre (CIRAD, UMR PVBMT) fue decisiva en la etapa del desarrollo del modelo.   

La interfaz del modelo R Shiny fue diseñada por <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (CIRAD, UMR PVBMT) con la ayuda de Frédéric Chiroleu (CIRAD, UMR PVBMT), Guillaume Cornu (CIRAD, UPR Forests and Societies) e Isaure Païtard (CIRAD, UMR PVBMT, ISTOM trainee) y contribuciones de Jean-Noël Aubertot (INRAe, UMR AGIR), Henri Brouchoud (CIRAD, UMR PVBMT), David Camilo Corrales (INRAe, UMR AGIR), Jean-Philippe Deguine (CIRAD, UMR PVBMT), Marie-Hélène Robin (EIP, UMR AGIR), Natacha Tabeuguia Motisi (CIRAD, UPR Bioagressors) y Marie-Anne Vedy-Zecchini (INRAe, UMR AGIR).  

La evaluación y validación del modelo con la interfaz Web IPSIM-chayote corrieron a cargo de <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (CIRAD, UMR PVBMT) e Isaure Païtard (CIRAD, UMR PVBMT, ISTOM trainee), que contaron con la participación imprescindible de productores de chayote: Yannick Soupapoullé (Arifel) y los productores de chayote de Salazie, recogidas por técnicos del sector: Eric Lucas, Eric Poulbassia (encargado de recuperar el retorno de los productores). 
También contribuyeron Luc Vanhuffel, Didier Vincenot (Cámara de agricultura de la Reunión), sin olvidar a Ludovic Maillary (DAAF Réunion) por la claridad de su visión global más distante.  

La transferencia de tecnología corrió a cargo de Eric Poulbassiay de sus colegas de la Cámara de Agricultura de la Reunión y de la organización Arifel que contribuyeron a la apropiación del modelo IPSIM-chayote por los productores, a través de sesiones de información y capacitación.  

La puesta en línea de la aplicación para computadoras y móviles fue obra de Henri Brouchoud (CIRAD, UMR PVBMT), Guillaume Cornu (CIRAD, UPR Forests and Societies) y <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (CIRAD, UMR PVBMT).  

Traducción al inglés por Andrew Hobson, al español por David Camilo Corrales, José Martin y Ana Llandres Lopez.  

> *¡GRACIAS A TODAS Y A TODOS por sus valiosas contribuciones que desde 2018 culminan, tras muchos esfuerzos, con el acceso abierto de IPSIM-chayote, herramienta "agroecológica"!*
