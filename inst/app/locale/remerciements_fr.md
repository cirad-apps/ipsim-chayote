<style>
a {color: black;}
</style>


## Dix années d’études sur la gestion agroécologique des Mouches du chouchou à La Réunion

Depuis 2009, des études ont été engagées sur la gestion agroécologique des Mouches des fruits (Tephritidae) sur le chouchou à La Réunion. A la fin des années 2000, la culture du chouchou, emblématique et patrimoniale, était menacée : les surfaces baissaient et les rendements chutaient, sous l’effet des mouches.  

Ces études se sont, pour la plupart, inscrites dans des projets de Recherche & Développement, rassemblant de nombreux partenaires, de la Recherche (Cirad, INRAe) au Développement (Chambre d’agriculture, Arifel, Organisations professionnelles), en passant par les organismes d’expérimentation et d’appui en protection des cultures (Armefhor, FDGDON, …). Les agriculteurs ont été les acteurs centraux de ces expériences agroécologiques et les organismes de développement et les pouvoirs publics ont joué un rôle déterminant dans le transfert des innovations.  

Les études ont permis de proposer des pratiques agroécologiques limitant fortement l’utilisation des pesticides sur le chouchou, dont la majorité des surfaces est cultivée en Agriculture Biologique aujourd’hui.  

Afin de former et d’informer les acteurs agricoles, un modèle de simulation des dégâts de mouches sur le chouchou a été conçu et mis au point depuis 2015.

## IPSIM-chouchou974 : un modèle conceptuel de simulation des dégâts des Mouches du chouchou

La construction du modèle IPSIM-chouchou974 a été coordonnée par Jean-Philippe Deguine (Cirad, UMR PVBMT).  

Les observations réalisées sur les dégâts des mouches ont été effectuées par le Cirad (UMR PVBMT), avec la collaboration de Cédric Ajaguin-Soleyen, Morguen Atiama, Toulassi Atiama-Nurbel, Jean-Philippe Deguine, Elisabeth Douraguia, Thomas François, Cathy Jacquard, Marie-Ludders Moutoussamy, Aubéri Petite, Pascal Rousse et Chloé Schmitt dans le cadre du projet Gamour. D’autres observations ont également été prises en compte, réalisées par la FDGDON (Marlène Marquier).  

La conception du modèle a été assurée par Jean-Philippe Deguine (Cirad, UMR PVBMT) et Jean-Noël Aubertot (INRAe, UMR AGIR). Ont également contribué : Toulassi Atiama-Nurbel (Armeflhor), Baptiste Longoras (GAB), Marlène Marquier (FDGDON) et Luc Vanhuffel (Chambre d’agriculture de La Réunion). Arnaud Allègre (Cirad, UMR PVBMT) a activement contribué à la réalisation du modèle.  

La conception, la création et la réalisation de l’interface Shiny ont été coordonnées par <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (Cirad, UMR PVBMT), avec l’aide de Frédéric Chiroleu (Cirad, UMR PVBMT), Guillaume Cornu (Cirad, UPR Forêts et Sociétés) et Isaure Païtard (Cirad, UMR PVBMT, stagiaire ISTOM). Ont également contribué : Jean-Noël Aubertot (INRAe, UMR AGIR), Henri Brouchoud (Cirad, UMR PVBMT), David Camilo Corrales (INRAe, UMR AGIR), Jean-Philippe Deguine (Cirad, UMR PVBMT), Marie-Hélène Robin (EIP, UMR AGIR), Natacha Tabeuguia Motisi (Cirad, UPR Bioagresseurs) et Marie-Anne Vedy-Zecchini (INRAe, UMR AGIR).  

L’évaluation et la validation du modèle doté de l’interface, IPSIM-chouchou974, ont été assurées par <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (Cirad, UMR PVBMT) et Isaure Païtard (Cirad, UMR PVBMT, stagiaire ISTOM), avec la contribution active des collègues ci-dessus, mais aussi et surtout avec l’aide des professionnels agricoles : Eric Lucas, Eric Poulbassia, Luc Vanhuffel, Didier Vincenot (Chambre d’agriculture de La Réunion), Yannick Soupapoullé (Arifel) et les producteurs de chouchou de Salazie. Ludovic Maillary (DAAF Réunion) a également apporté son éclairage sur le modèle.  

Ces collègues de la Chambre d’agriculture de La Réunion et de l’Arifel ont ensuite activement contribué au transfert et à l’appropriation du modèle IPSIM-chouchou974 par les agriculteurs, via des sessions de formation et de sensibilisation.  

La mise en ligne de l’application a été réalisé par Henri Brouchoud (Cirad, UMR PVBMT), Guillaume Cornu (Cirad, UPR Forêts et Sociétés) et <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (Cirad, UMR PVBMT).  

La traduction en anglais a été assurée par Andrew Hobson, celle en espagnol par David Camilo Corrales, José Martin et Ana Llandres Lopez.  

> *Que toutes les contributrices et tous les contributeurs soient ici remercié(e)s !*
