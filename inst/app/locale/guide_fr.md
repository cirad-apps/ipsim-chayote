#### Dans l’onglet "Vos données" :

1.  Répondez aux 15 questions qui concernent vos pratiques agricoles,
    votre parcelle et son environnement.  
2.  Validez.  
3.  Vous serez redirigé automatiquement vers l’onglet "Votre résultat".

------------------------------------------------------------------------

#### *Vos données servent à produire le résultat et ne sont pas conservées.*

------------------------------------------------------------------------

#### Dans l’onglet "Votre résultat" :

1.  Dans la partie supérieure, l’échelle renseigne l’intensité estimée
    des dégâts des mouches sur les chouchous.  
2.  Dans la partie inférieure, le schéma permet d’indiquer l’origine des
    dégâts.
