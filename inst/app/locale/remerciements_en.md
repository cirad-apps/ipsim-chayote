<style>
a {color: black;}
</style>


## Ten years of studies on the agroecological management of chayote crop fruit flies in Reunion

Since 2009, studies have been carried out on the agroecological management of fruit flies (Tephritidae) on chayote crops in Reunion. At the end of the 2000s, the emblematic chayote crop was threatened: plots with devastating damage from fruit flies, production areas were dwindling and yields were falling.  

Most of these studies were part of Research & Development projects bringing together partners from Research (CIRAD, INRAe) to Development (Chamber of Agriculture, Arifel, Professional Organizations), as well as crop protection and support organizations (Armefhor, FDGDON).
Farmers are central stakeholders in these agroecological experiences, and development agencies and governments have been instrumental in transferring innovations.  

These studies have made it possible to implement agroecological practices which use only a minimal amount of pesticides on chayote. Today, the majority of this crop is cultivated organically.  

In order to train and inform the agricultural stakeholders, a simulation model of fruit fly damage to chayote has been in development since 2015.

## IPSIM-chayote: A Conceptual Model of Fruit Fly Damage to Chayote

The development of the IPSIM-chayote model was coordinated by Jean-Philippe Deguine (CIRAD, UMR PVBMT).  

The observations of fly damage were made by CIRAD (UMR PVBMT), with the collaboration of Cédric Ajaguin-Soleyen, Morguen Atiama, Toulassi Atiama-Nurbel, Jean-Philippe Deguine, Elisabeth Douraguia, Thomas François, Cathy Jacquard, Marie-Ludders Moutoussamy, Aubéri Petite, Pascal Rousse and Chloé Schmitt as part of the Gamour project. 
Other observations were also made by FDGDON (Marlène Marquier).  

The model was designed by Jean-Philippe Deguine (CIRAD, UMR PVBMT) and Jean-Noël Aubertot (INRAe, UMR AGIR). Also contributing: Toulassi Atiama-Nurbel (Armeflhor), Baptiste Longoras (GAB), Marlene Marquier (FDGDON) and Luc Vanhuffel (Chamber of Agriculture of Reunion). 
Arnaud Allègre (CIRAD, UMR PVBMT) actively contributed to the development of the model.  

The design and creation of the Shiny interface was coordinated by <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (CIRAD, UMR PVBMT), with the help of Frédéric Chiroleu (CIRAD, UMR PVBMT), Guillaume Cornu (CIRAD, UPR Forests and Societies) and Isaure Païtard (CIRAD, UMR PVBMT, ISTOM trainee). 
Also contributing: Jean-Noël Aubertot (INRAe, UMR AGIR), Henri Brouchoud (CIRAD, UMR PVBMT), David Camilo Corrales (INRAe, UMR AGIR), Jean-Philippe Deguine (CIRAD, UMR PVBMT) and Marie-Hélène Robin (EIP, UMR AGIR), Natacha Tabeuguia Motisi (CIRAD, UPR Bioagressors) and Marie-Anne Vedy-Zecchini (INRAe, UMR AGIR).  

The evaluation and validation of the model with the IPSIM-chayote interface was provided by <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (CIRAD, UMR PVBMT) and Isaure Païtard (CIRAD, UMR PVBMT, ISTOM trainee), with the help of the following agricultural professionals: Eric Lucas, Eric Poulbassia who coordinated the exchnages with farmers, Luc Vanhuffel, Didier Vincenot (Reunion Chamber of Agriculture), Yannick Soupapoullé (Arifel) and Salazie’s chayote producers. 
Ludovic Maillary (DAAF Réunion) also worked on the model.  

These colleagues from the Reunion Chamber of Agriculture, especially Eric Poulbassia, and Arifel actively contributed to the transfer and adoption of the IPSIM-chayote model by farmers through training and awareness sessions.  

The application was made available online by Henri Brouchoud (CIRAD, UMR PVBMT), Guillaume Cornu (CIRAD, UPR Forests and Societies) and <a href="https://doana-r.gitlab.io/" target="_blank">Anna Doizy</a> (CIRAD, UMR PVBMT).  

Translation into English by Andrew Hobson and into Spanish by David Camilo Corrales, José Martin and Ana Llandres Lopez.  

> *Thanks to all contributors!*

