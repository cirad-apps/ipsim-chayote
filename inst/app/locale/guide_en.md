#### In the "Your data" tab:

1.  Answer the questions about your farming practices, your farm and its
    environment.  
2.  Validate.  
3.  You will be automatically redirected to the "Your result" tab.

------------------------------------------------------------------------

#### *Your data is used to produce the result and is not retained.*

------------------------------------------------------------------------

#### In the "Your results" tab:

1.  The scale in the upper area shows the estimated intensity of fly
    damage to chayote.  
2.  The diagram in the lower area indicates the origin of the damage.
