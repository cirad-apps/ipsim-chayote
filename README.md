
<!-- README.md is generated from README.Rmd. Please edit that file -->

<img src='inst/app/www/logo_en.png' height="150px" align="middle" style="padding: 10px;"/>

<!-- badges: start -->

[![Lifecycle:
stable](https://img.shields.io/badge/lifecycle-stable-brightgreen.svg)](https://www.tidyverse.org/lifecycle/#stable)
<!-- badges: end -->

**IPSIM-chayote** is a tool developped by CIRAD for professionals in
agriculture to predict, and manage the damage caused by fruit flies on
the chayote in Reunion island.

------------------------------------------------------------------------

-   Current version: 0.1.0
-   Contact email: <ipsim-chayote@cirad.fr>  
-   Developped at CIRAD by [Anna Doizy](doana-r.com), Isaure Païtard,
    Frederic Chiroleu and Jean-Philippe Deguine  
-   Associated publication: [Qualitative modeling of fruit fly injuries
    on chayote in Réunion: Development and transfer to
    users](https://doi.org/10.1016/j.cropro.2020.105367)

## Online usage

Just go to <https://pvbmt-apps.cirad.fr/apps/ipsim-chayote/?lang=en>.

English, French and Spanish versions are available.

## Local usage

You can install **IPSIM-chayote** R package from
[gitlab](https://gitlab.com/cirad-apps/ipsim-chayote) with:

``` r
remotes::install_gitlab("cirad-apps/ipsim-chayote@0.1.0")
```

Run this command to launch the app locally:

``` r
library(ipsimchayote)
run_app()
```
