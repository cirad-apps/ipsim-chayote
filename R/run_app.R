#' Run phylostems application
#'
#'
#' @export
run_app <- function() {
  shiny::runApp(system.file("app", package = "ipsimchayote"), launch.browser = T)
}
